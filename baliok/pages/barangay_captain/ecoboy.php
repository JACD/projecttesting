<?php include '../../include/mainincludetop.php';?>
 

					<!-- Page Heading -->

					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="h3 mb-0 text-success-800">Approval of residents</h1>
					</div>
					
					
					<!-- DREA E SULOD ANG CONTENT -->
					
					   <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <center><h6 class="m-0 font-weight-bold text-gray-600">Garbage Collection Schedule</h6></center>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i> Create Schedule</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                  <th>Collection Date</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>Purok No.</th>
                      <th>Ecoboy Name</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Collection Date</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>Purok No.</th>
                      <th>Ecoboy Name</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <tr>
                    <?php 
					$result = mysqli_query($db,"SELECT * FROM schedule where status='inactive';");

                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                     <td><?php echo $row['CollDate'];?></td>
                      <td><?php echo $row['StartTime'];?></td>
                      <td><?php echo $row['EndTime'];?></td>
                      <td><?php echo $row['Purok_No'];?></td>
                      <td><?php echo $row['EcoboyName'];?></td> 
                      <td><button id="<?php echo $row['Sched_No'];?>"class="btn btn-success" onclick="updateOfficial(this);" data-toggle="modal" data-target="#editModal"><i class="fas fa-pencil"></button></td>
                      <td><a href="delete_brgycaptain.php?Sched_No=<?php echo $row['Sched_No'];?>"><button class="btn btn-danger"><i class="fas fa-trash"></td>  
                    </tr>
                     
                    <?php
                    }
                    ?> 
                  </tbody>
                </table>
              </div>
            </div>
          </div>
					
					
					
					
					   <!-- Add Schedule Modal -->
    <form action="../../php_function/add_schedule.php" role="form" method="POST">
        <div class="modal" id="myModal">
          <div class="modal-dialog">
            <div class="modal-content">

              <!-- Modal Header -->
              <div class="modal-header">
                <center><h4 class="modal-title">Garbage Collection Schedule</h4></center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <!-- Modal body -->
              <div class="modal-body">
                <div class="form-group">
                <label>Date</label>
                <input type="date" class="form-control" id="date" name="CollDate" required>
                </div>
                <div class="form-group">
                <label>Start Time</label>
                <input type="time" class="form-control" id="start" name="StartTime" required>
                </div>
                <div class="form-group">
                  <label>End Time</label>
                  <input type="time" class="form-control" id="end" name="EndTime" required>
                </div>
                <div class="form-group">
                <label>Purok No.:</label>
                <select class="form-control select2" id="purok"name=" Purok_No" required>
                  <option></option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="RV3 7-A">RV3 7-A</option>
                  <option value="RV3 7-B">RV3 7-B</option>
                  <option value="RV3 8-A">RV3 8-A</option>
                  <option value="RV3 8-B">RV3 8-B</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11-A">11-A</option>
                  <option value="11-B">11-B</option>
                </select>
                </div>
                <div class="form-group">
                <label>Ecoboy Name:</label>
                <select class="form-control select2 form-control" id="ecoboy" name="EcoboyName" required>
                  <option></option>
                  <option value="Rogelio Ompod">Rogelio Ompod</option>
                  <option value="Fernando Ompod">Fernando Ompod</option>
                  <option value="Fernando Ompod Jr.">Fernando Ompod Jr.</option>
                  <option value="Rodelio Olaco">Rodelio Olaco</option>
                  <option value="Nilo Maganang">Nilo Maganang</option>
                  <option value="Robert Amigo">Robert Amigo</option>
                </select>
                </div>
              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="submit" name="btn-sked" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </div>
      </form>
					
					<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->

 

	<?php include '../../include/mainincludebottom.php';?>
	 

				 