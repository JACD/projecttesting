<?php include '../../include/mainincludetop.php';?>
 

					<!-- Page Heading -->

					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="h3 mb-0 text-success-800">Declined residents</h1>
					</div>
					
 <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            <center><h6 class="m-0 font-weight-bold text-gray">Residents</h6></center>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Purok No</th>
                      <th>Email</th>
                      <th>Approve</th>
                      <th>Decline</th>
                      <th>Profile</th>
                    </tr>
                  </thead>
               
                  <tbody>
                   
                    <?php
                    
                    $f = mysqli_query($db,"SELECT * FROM residence where status='decline';");
                    while($row = mysqli_fetch_array($f))
                    {   
                      ?>
                       <tr>
                      <td> <?php echo $row['lname'];?> </td>
                      <td> <?php echo $row['fname'];?> </td>
                      <td> <?php echo $row['mname'];?> </td>
                      <td> <?php echo $row['Purok_No'];?> </td>
                      <td> <?php echo $row['email'];?> </td>
                      <td><a href="../../php_function/accept.php?ayde=<?php echo $row['id'];?>"><button class="btn btn-info"><i class="fas fa-user-check"></button></a></td>
                      <td> <a href="../../php_function/decline.php?ayde=<?php echo $row['id'];?>"><button class="btn btn-danger"><i class="fas fa-user-times"></button></a></td>
                      <td> <a href="../../pages/barangay_captain/profile.php?ayde=<?php echo $row['id'];?>"><button class="btn btn-success"><i class="fas fa-eye"></button></a></td>
                      </tr>

                    <?php
                    }
                    ?> 
                                       
                  </tbody>
                     <tfoot>
                    <tr>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Purok No</th>
                      <th>Email</th>
                      <th>Approve</th>
                      <th>Decline</th>
                      <th>Profile</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>



					<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->

 

	<?php include '../../include/mainincludebottom.php';?>
	 

				 