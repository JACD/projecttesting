    <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../../home.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <img src="../../components/img/logo/logo.png" height="60" width="60">
        </div>
        <div class="sidebar-brand-text mx-3">Barangay Baliok</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="./">
          <i class="fas fa-fw fa-home"></i>
          <span>Home</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
	  
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users"></i>
          <span>Residence</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Residence</h6>
            <a class="collapse-item" href="../../pages/barangay_captain/residence_r.php">Resident list</a>
            <a class="collapse-item" href="../../pages/barangay_captain/residence_a.php">Approval of residents</a>
            <a class="collapse-item" href="../../pages/barangay_captain/residence_d.php">Declined residents</a> 
 
          </div>
        </div>
      </li> 

      <!-- Nav Item - Ecoboy -->
      <li class="nav-item">
        <a class="nav-link" href="../../pages/barangay_captain/ecoboy.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Ecoboy</span></a>
      </li>
 
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->