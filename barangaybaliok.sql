-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2019 at 05:20 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barangaybaliok`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomplishment_report`
--

CREATE TABLE `accomplishment_report` (
  `Accomp_No` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Plate_No` varchar(50) NOT NULL,
  `Destination` varchar(50) NOT NULL,
  `Commodity` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `brgycaptain`
--

CREATE TABLE `brgycaptain` (
  `brgycaptain_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` date NOT NULL,
  `yearend` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brgycaptain`
--

INSERT INTO `brgycaptain` (`brgycaptain_id`, `name`, `yeardeclared`, `yearend`) VALUES
(1, 'Select from the list', '2019-04-16', '2019-04-09');

-- --------------------------------------------------------

--
-- Table structure for table `brgykagawad`
--

CREATE TABLE `brgykagawad` (
  `brgykagawad_id` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` date NOT NULL,
  `yearend` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brgykagawad`
--

INSERT INTO `brgykagawad` (`brgykagawad_id`, `rank`, `name`, `yeardeclared`, `yearend`) VALUES
(1, 2, '', '2019-04-09', '2019-04-24'),
(2, 5, '', '2019-04-09', '2019-04-23');

-- --------------------------------------------------------

--
-- Table structure for table `brgypersonnel`
--

CREATE TABLE `brgypersonnel` (
  `personnel_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `brgyposition` varchar(50) NOT NULL,
  `yeardeclared` date NOT NULL,
  `yearend` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `residence`
--

CREATE TABLE `residence` (
  `id` int(11) NOT NULL,
  `Brgy_ID` varchar(15) NOT NULL,
  `Precinct_No` varchar(10) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `suffix` varchar(5) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `capability` varchar(25) DEFAULT NULL,
  `assistedby` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` int(5) NOT NULL,
  `province` varchar(50) NOT NULL,
  `citymuni` varchar(50) NOT NULL,
  `barangay` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `noofyears` int(5) NOT NULL,
  `noofmonths` int(5) NOT NULL,
  `numofyears` int(5) NOT NULL,
  `citizenship` varchar(25) NOT NULL,
  `citizenshipby` varchar(25) NOT NULL,
  `dob` date NOT NULL,
  `cmuni` varchar(50) NOT NULL,
  `cprovince` varchar(50) NOT NULL,
  `don` date NOT NULL,
  `cnoa` varchar(25) NOT NULL,
  `rdon` date NOT NULL,
  `cnooa` varchar(25) NOT NULL,
  `occupation` varchar(25) NOT NULL,
  `tin` varchar(15) NOT NULL,
  `civilstatus` varchar(20) NOT NULL,
  `spouse` varchar(25) NOT NULL,
  `flname` varchar(50) NOT NULL,
  `ffname` varchar(50) NOT NULL,
  `fmname` varchar(50) NOT NULL,
  `mlname` varchar(50) NOT NULL,
  `mfname` varchar(50) NOT NULL,
  `mmname` varchar(50) NOT NULL,
  `hotf` varchar(50) NOT NULL,
  `hotfname` int(50) NOT NULL,
  `names` varchar(500) NOT NULL,
  `Purok_No` varchar(15) NOT NULL,
  `purokleader` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `re_pass` varchar(50) NOT NULL,
  `status` enum('inactive','active') NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residence`
--

INSERT INTO `residence` (`id`, `Brgy_ID`, `Precinct_No`, `lname`, `fname`, `mname`, `suffix`, `nickname`, `capability`, `assistedby`, `gender`, `height`, `weight`, `province`, `citymuni`, `barangay`, `street`, `noofyears`, `noofmonths`, `numofyears`, `citizenship`, `citizenshipby`, `dob`, `cmuni`, `cprovince`, `don`, `cnoa`, `rdon`, `cnooa`, `occupation`, `tin`, `civilstatus`, `spouse`, `flname`, `ffname`, `fmname`, `mlname`, `mfname`, `mmname`, `hotf`, `hotfname`, `names`, `Purok_No`, `purokleader`, `email`, `password`, `re_pass`, `status`, `photo`) VALUES
(9, '19-000', '0000-0', 'lastname', 'firstname', 'middlename', 'Jr.', 'nickname', '', '', 'Male', '52', 50, 'province', 'city', 'barangay', 'house', 6, 6, 6, 'citizenship', '', '0000-00-00', '', '', '0000-00-00', '', '0000-00-00', '', 'none', '000-000-000', ' Single', '', 'lastname', 'firstname', 'middlename', 'lastname', 'firstname', 'middlename', 'Yes', 0, 'add,adda,addaa', '10', 'Vicentita Ocay', 'wer@gmail.com', 'wer', 'wer', 'active', 'user.png');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `Sched_No` int(11) NOT NULL,
  `CollDate` date NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Purok_No` varchar(10) NOT NULL,
  `EcoboyName` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`Sched_No`, `CollDate`, `StartTime`, `EndTime`, `Purok_No`, `EcoboyName`, `status`, `date_created`) VALUES
(5, '2019-04-01', '03:45:00', '15:45:00', 'RV3 8-A', 'Nilo Maganang', 'inactive', '2019-04-15 02:55:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomplishment_report`
--
ALTER TABLE `accomplishment_report`
  ADD PRIMARY KEY (`Accomp_No`);

--
-- Indexes for table `brgycaptain`
--
ALTER TABLE `brgycaptain`
  ADD PRIMARY KEY (`brgycaptain_id`);

--
-- Indexes for table `brgykagawad`
--
ALTER TABLE `brgykagawad`
  ADD PRIMARY KEY (`brgykagawad_id`);

--
-- Indexes for table `brgypersonnel`
--
ALTER TABLE `brgypersonnel`
  ADD PRIMARY KEY (`personnel_id`);

--
-- Indexes for table `residence`
--
ALTER TABLE `residence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`Sched_No`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomplishment_report`
--
ALTER TABLE `accomplishment_report`
  MODIFY `Accomp_No` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brgycaptain`
--
ALTER TABLE `brgycaptain`
  MODIFY `brgycaptain_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brgykagawad`
--
ALTER TABLE `brgykagawad`
  MODIFY `brgykagawad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `brgypersonnel`
--
ALTER TABLE `brgypersonnel`
  MODIFY `personnel_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `residence`
--
ALTER TABLE `residence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `Sched_No` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
